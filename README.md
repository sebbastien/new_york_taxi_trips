# New_york_taxi_trips

Data integration, Data cleaning and analysing the New York City Taxi and Limousine Commission (or TLC for short) records about taxi trips in New York since 2009.

# Data

The complete dataset, as well as documents that describe these data, is available on the following website: https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page

The TLC trip dataset actually consists of 4 sub-datasets:

- Yellow taxi records are records that record trip information of New York's famous yellow taxi cars.

- Green taxi records are records that record trip information by so-called 'boro' taxis a newer service introduced in August of 2013 to improve taxi service and availability in the boroughs.

- FHV records (short for 'For Hire Vehicles') record information from services that offer for-hire vehicles (such as Uber, Lyft, Via, and Juno), but also luxury limousine bases.

- High volume FHV (FHVHV for short) are FHV records oered by services that make
more than 10,000 trips per day.


For a complete description of these sub-dataset types, see https://www1.nyc.gov/assets/tlc/downloads/pdf/trip_record_user_guide.pdf and the links therein.

# Licence
(c) Copyright 2021 Sebastien Le clec'h.

Licensed under the MIT license:

    http://www.opensource.org/licenses/mit-license.php
